package learna.backend.repository;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import learna.backend.entities.TutorToSubject;

import java.util.List;
import java.util.UUID;

@Repository
public interface TutorToSubjectRepository extends CrudRepository<TutorToSubject, UUID> {

    List<TutorToSubject> findByTutorId(UUID tutorId);
}
