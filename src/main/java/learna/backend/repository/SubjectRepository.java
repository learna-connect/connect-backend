package learna.backend.repository;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import learna.backend.entities.Subject;

import java.util.List;
import java.util.UUID;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, UUID> {

    List<Subject> findByName(String name);
}
