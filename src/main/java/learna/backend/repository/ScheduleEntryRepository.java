package learna.backend.repository;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;
import learna.backend.entities.ScheduleEntry;

import java.util.List;

@Repository
public interface ScheduleEntryRepository extends CrudRepository<ScheduleEntry,Long> {

    List<ScheduleEntry> findByUserId(String userId);
}
