package learna.backend.beans;

import io.micronaut.core.annotation.Introspected;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nullable;

@Introspected
@Getter
@Setter
public class SearchArgs {

    @Nullable
    private String name;
}
