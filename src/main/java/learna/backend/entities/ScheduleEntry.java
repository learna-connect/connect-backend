package learna.backend.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "schedule_entry")
@Data
public class ScheduleEntry {


    @Column
    @Id
    @GeneratedValue
    private Long id;


    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    private LocalTime endTime;

    @Column
    private LocalDate date;

    @Column(name = "creator")
    private String creator;


    @Column
    private String attendee;


}
