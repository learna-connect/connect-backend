package learna.backend.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "subjects_enrolled")
@Data
public class TutorToSubject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 16)
    private UUID id;

    @Column(name = "subject_id")
    private UUID subjectId;

    @Column(name = "tutor_id")
    private UUID tutorId;


}
