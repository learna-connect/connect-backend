package learna.backend.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "subject")
@Data
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 16)
    private UUID id;

    @Column(length = 50)
    private String name;


    @Column(length = 30)
    private String code;
}
