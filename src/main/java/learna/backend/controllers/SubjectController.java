package learna.backend.controllers;


import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import learna.backend.beans.SearchArgs;
import learna.backend.entities.Subject;
import learna.backend.entities.TutorToSubject;
import learna.backend.repository.SubjectRepository;
import learna.backend.repository.TutorToSubjectRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller("/subjects")
public class SubjectController {


    private final SubjectRepository subjectRepository;

    private final TutorToSubjectRepository toSubjectRepository;


    public SubjectController(SubjectRepository subjectRepository, TutorToSubjectRepository toSubjectRepository) {
        this.subjectRepository = subjectRepository;
        this.toSubjectRepository = toSubjectRepository;
    }


    //TODO: review
    @Post
    public Subject addSubject(@Body @Valid Subject subject){
        return subjectRepository.save(subject);
    }

    @Get("{?args*}")
    public Iterable<Subject> getAllSubjects(@Valid SearchArgs args){
        if(args.getName() != null){
            return subjectRepository.findByName(args.getName());
        }
        return subjectRepository.findAll();
    }

    @Get("/{subjectId}")
    public Optional<Subject> getSubjectDetails(String subjectId){
        return subjectRepository.findById(UUID.fromString(subjectId));
    }


    @Post("/tutor")
    public void setTutorSubject(@Body @Valid TutorToSubject enrolled){
        toSubjectRepository.save(enrolled);
    }

    @Get("/tutor/{tutorId}")
    public List<Subject> getTutoringSubject(String tutorId){

        List<TutorToSubject> list = toSubjectRepository.findByTutorId(UUID.fromString(tutorId));

        List<Subject> subjects = new ArrayList<>();

        list.forEach(item -> {
            Optional<Subject> subject = subjectRepository.findById(item.getSubjectId());

            subject.ifPresent(subjects::add);
        });

        return subjects;
    }
}
