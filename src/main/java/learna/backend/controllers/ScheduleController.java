package learna.backend.controllers;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import learna.backend.entities.ScheduleEntry;
import learna.backend.repository.ScheduleEntryRepository;

import javax.validation.Valid;
import java.util.List;

@Controller("/schedule")
public class ScheduleController {


    private final ScheduleEntryRepository repository;

    public ScheduleController(ScheduleEntryRepository repository) {
        this.repository = repository;
    }

    @Post("/{userId}")
    public void postScheduleEntry(String userId, @Body @Valid ScheduleEntry scheduleEntry){
            repository.save(scheduleEntry);
    }

    @Get("/{userId}")
    public List<ScheduleEntry> findAllUserIdEntries(String userId){
        return repository.findByUserId(userId);
    }
}
